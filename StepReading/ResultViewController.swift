//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, NADViewDelegate {
    private var nadView: NADView!
    var deciCounter:Double = 0
    var globalFirstLabel:String = ""
    var globalSecondLabel:String = ""
    var globalThirdLabel:String = ""
    var firstGlobalEnglishLabel:String = ""
    var pauseBool:Bool = false
    var firstShowCounter:Int = 0
    var lastChangeSpeed:Double = 0
    var finishBool:Bool = false
    var globalRandomTopic:Int = 0
    var canCorrect:Bool = true
    var canPicker:Bool = true
    var topicArrayNumber:Int = 0
    var correctTotal:Int = 0
    var correctNumber:Int?
    var topicTextCounter:Int = 0
    var resolveTopic = false
    var randomAnswer = 0
    var numberTopic:Int = 0
    var timer:NSTimer!
    var correctIntervalTimer:NSTimer!
    var speedTimer:NSTimer!
    var speedInternalTimer:NSTimer!
    var correctTopicTimer:NSTimer!
    var adTimer:NSTimer!
    var deciTimer:NSTimer!
    var startCount = true
    var counter:Double = 0
    var totalCounter:Double = 0.0
    var touchCounter:Bool = false
    var random1:Int = 0
    var random2:Int = 0
    var randomArray:[[Int]] = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
    var answerTopic:String = ""
    var candidateTopic1:String = ""
    var candidateTopic2:String = ""
    var randomTopic1 = 0
    var randomTopic2 = 0
    var randomTopic3 = 0
    var randomTopic4 = 0
    var randomTopic5 = 0
    var randomTopic6 = 0
    var randomTopic7 = 0
    var randomTopic8 = 0
    var randomTopic9 = 0
    var randomTopic10 = 0
    let texts:NSArray = ["0.1s", "0.5s", "1.0s", "2.0s"]
    var speedLevel:Double = 0.1
    var arrayCharacter = [String]()
    var countArray = [Int]()
    var tmpCounter = [Double]()
    var colour = [UIColor]()
    var myString = [NSMutableAttributedString]()
    var totalTimeCounter = [Double]()
    var tmpTotalTimeCounter = Int()
    var globalEnglishLabel = [NSMutableAttributedString]()
    var mainRow:Int = 0
    var rowArray:[[String]] = [["","","",""], ["","","",""]]
    var randomTopic:[Int] = []
    var topicArray:[[String]] = [["",""], ["",""]]
    var topicArrayAnswer:[[String]] = [["",""], ["",""]]
    var currentTopicArray:[[String]] = [["",""], ["",""]]
    var firstLabel:String = ""
    var secondLabel:String = ""
    var thirdLabel:String = ""
    
    
    
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var englishLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var firstBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var secondBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var thirdBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var topicLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var correctDecisionLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var showCountSecond: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var resultBtn: UIButton!
    @IBOutlet weak var topicCounter: UILabel!
    @IBOutlet weak var uiSlider: UISlider!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBAction func touchFirstBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if (speedTimer != nil) {
            speedTimer.invalidate()
        }
        if (speedInternalTimer != nil) {
            speedInternalTimer.invalidate()
        }
        if (correctTopicTimer != nil) {
            correctTopicTimer.invalidate()
        }
        if (deciTimer != nil) {
            deciTimer.invalidate()
        }
        if pauseBool == false {
            if numberTopic > 10 {
                finishBool = true
                pickerView.hidden = false
                resultBtn.hidden = false
                startButton.hidden = false
                pauseLabel.hidden = true
                resultBtn.setTitle("result ☞", forState: .Normal)
                timer.invalidate()
                canPicker = true
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                numberTopic = 0
            }
            if canCorrect == true && finishBool == false {
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                if correctNumber == 1 {
                    correctTotal++
                    correctDecisionLabel.text = "◯"
                } else {
                    correctDecisionLabel.text = "×"
                }
                topicArrayNumber = 0
                counter = 0
                resolveTopic = true
                correctIntervalTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("correctDecistionHolds"), userInfo: nil, repeats: true)
            }
            canCorrect = false
        }
    }
    @IBAction func touchSecondBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if (speedTimer != nil) {
            speedTimer.invalidate()
        }
        if (speedInternalTimer != nil) {
            speedInternalTimer.invalidate()
        }
        if (correctTopicTimer != nil) {
            correctTopicTimer.invalidate()
        }
        if (deciTimer != nil) {
            deciTimer.invalidate()
        }
        if pauseBool == false {
            if numberTopic > 10 {
                finishBool = true
                pickerView.hidden = false
                resultBtn.hidden = false
                startButton.hidden = false
                pauseLabel.hidden = true
                resultBtn.setTitle("result ☞", forState: .Normal)
                timer.invalidate()
                canPicker = true
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                numberTopic = 0
            }
            if canCorrect == true && finishBool == false {
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                if correctNumber == 2 {
                    correctTotal++
                    correctDecisionLabel.text = "◯"
                } else {
                    correctDecisionLabel.text = "×"
                }
                topicArrayNumber = 0
                counter = 0
                resolveTopic = true
                correctIntervalTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("correctDecistionHolds"), userInfo: nil, repeats: true)
            }
            canCorrect = false
        }
    }
    @IBAction func touchThirdBtn(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if (speedTimer != nil) {
            speedTimer.invalidate()
        }
        if (speedInternalTimer != nil) {
            speedInternalTimer.invalidate()
        }
        if (correctTopicTimer != nil) {
            correctTopicTimer.invalidate()
        }
        if (deciTimer != nil) {
            deciTimer.invalidate()
        }
        if pauseBool == false {
            if numberTopic > 10 {
                finishBool = true
                pickerView.hidden = false
                resultBtn.hidden = false
                startButton.hidden = false
                pauseLabel.hidden = true
                resultBtn.setTitle("result ☞", forState: .Normal)
                timer.invalidate()
                canPicker = true
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                numberTopic = 0
            }
            if canCorrect == true && finishBool == false {
                if (timer != nil) {
                    timer.invalidate()
                }
                if (speedTimer != nil) {
                    speedTimer.invalidate()
                }
                if (speedInternalTimer != nil) {
                    speedInternalTimer.invalidate()
                }
                if (correctTopicTimer != nil) {
                    correctTopicTimer.invalidate()
                }
                if correctNumber == 3 {
                    correctTotal++
                    correctDecisionLabel.text = "◯"
                } else {
                    correctDecisionLabel.text = "×"
                }
                topicArrayNumber = 0
                counter = 0
                resolveTopic = true
                correctIntervalTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("correctDecistionHolds"), userInfo: nil, repeats: true)
            }
            canCorrect = false
        }
    }
    @IBAction func pauseButton(sender: UIButton) {
        if (timer != nil) {
            timer.invalidate()
        }
        if (speedTimer != nil) {
            speedTimer.invalidate()
        }
        if (speedInternalTimer != nil) {
            speedInternalTimer.invalidate()
        }
        if (correctTopicTimer != nil) {
            correctTopicTimer.invalidate()
        }
        if (correctIntervalTimer != nil) {
            correctIntervalTimer.invalidate()
        }
        if (deciTimer != nil) {
            deciTimer.invalidate()
        }
        topicLabel.text = ""
        startButton.hidden = false
        pauseLabel.hidden = true
        topicArrayNumber = 0
        pauseBool = true
        numberTopic--
    }
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func saveLearning(sender: UIButton) {
    }
    @IBAction func startSentences(sender: UIButton) {
        deciCounter = 0
        pauseBool = false
        canCorrect = true
        pickerView.hidden = true
        canPicker = false
        if numberTopic == 0 {
            correctTotal = 0
            resultBtn.hidden = true
            resultBtn.setTitle("", forState: .Normal)
        }
        topicLabel.text = ""
        topicCounter.hidden = false
        startButton.hidden = true
        pauseLabel.hidden = false
        resultBtn.hidden = true
        firstBtn.hidden = false
        secondBtn.hidden = false
        thirdBtn.hidden = false
        startTopic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)

        englishLabelWidth.constant = myBoundSize.width - 20
        firstBtnWidth.constant = myBoundSize.width - 20
        secondBtnWidth.constant = myBoundSize.width - 20
        thirdBtnWidth.constant = myBoundSize.width - 20
        topicLabelWidth.constant = 300
        resultBtn.hidden = true
        resultBtn.setTitle("", forState: .Normal)
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        firstBtn.titleLabel!.numberOfLines = 0
        secondBtn.titleLabel!.numberOfLines = 0
        thirdBtn.titleLabel!.numberOfLines = 0
        progressView.progress = 0
        
        topicArray = [
            [
                "Hello, Mr.Skyler. This is Janice Hartnet from EZ Electronics.",
                "I'm calling to inform you that the laptop computer you dropped off last week has been successfully repaired.",
                "We were able to undo the damage caused by the virus that infected your compute, and while most of the data on your hand drive was lost in the process, we were able to back it up completely.",
                "You can come in and pick it up anytime between 9:30 A.M and 8:30P.M., Monday through Saturday, or on Sunday between 11 A.M. and 6 P.M.",
                "If you prefer to use our safe and timely delivery service at no extra cost, please call me at 208-555-3290.",
                "Thank you, and have a good day."],
            [
                "Welcome to 'Dollars and Sense'.",
                "I'm your host, Amanda Kelly.",
                "Today's topic is how to be responsible with your credit and how to avoid debt.",
                "The first rule is to never use your credit card unless you have the cash to back it up.",
                "If you can't pay off your credit card bill by the time your next paycheck comes in,  you may want to hold off on marketing a purchase.",
                "If you do have substantial debt on your card, make sure you pay off more than minimum requirement each month, or else you'll never beat down that interest.",
                "And finally, unless it's an emergency, never use your credit card to withdraw cash.",
                "The interest on cash withdrawals is quite high.",
                "Join us tomorrow when we discuss applying for a loan."],
            [
                "I'd like to thank you all for choosing to participate in next week's Annual City Charity Cycle Race.",
                "I want to thank you for signing up to race, and for finding sponsors that are willing to contribute to a worthy cause.",
                "As you all have already checked in with our race committee, I will first go over the rules for the race.",
                "There are only a few rules I need to explain, and then we'll follow that with distributing and explaining the course maps.",
                "Please note where the water stands and bicycle maintenance crews are stationed throughout the course.",
                "We'll finish off today's meeting with a question and answer period."],
            [
                "I've just received word from the president that the merger between our company and World moving has been finalized.",
                "You can all rest assured that there will be no downsizing of staff.",
                "The only change will be that World Moving will take on this company's name and be known as Allied Services.",
                "Although we are keeping on their staff as well, we will need to train them in proper Allied Services procedures.",
                "That is why staring next Monday, for one week, you all will be sent to World moving's office to train their staff.",
                "Are there any questions?"],
            [
                "I know this is your first day of training here at Wonderware, so I'll take things slowly, and if you have any questions, just stop me along the way.",
                "Each of you will be assigned a workstation for tracking products as they leave the factory and are sent to our regional distributors.",
                "At your workstation, you must first insert your company ID card into the computer or else you won't be able to use it.",
                "Remember to take your ID card with you leave your station for the day.",
                "We've had problems with people forgetting them in the past.",
                "The best way to explain how everything works is to have you actually do it, so let's get all of you to a workstation and we can begin."],
            [
                "Good afternoon.",
                "My name is Alicia Fenwick, and I'm the founder and owner of Pocket Furniture, a small family-owned business in downtown Waterloo.",
                "I'm also the president of the Toronto Small Business Association.",
                "As owners of successful small businesses, I'd like to ask you to join our association.",
                "Some of the benefits of joining include discounts on payroll and office supplies, and we all know how discounts can be as good as free money.",
                "You'll also have access to programs that offer pension plans and health insurance to your employees.",
                "In addition, we also have a team of legal professionals that can help you with any labor or contract disputes."],
            [
                "Attention all passengers waiting to board the 9:15 train for Milwaukee.",
                "We apologize for the inconvenience, but all trains along the Cedar Point-Milwaukee line have been canceled because of severe snowstorms along the route.",
                "It is not certain when trains on the Ceder Point-Milwaukee line will resume service.",
                "Passengers with tickets for this line may exchange them for a full refund.",
                "Passengers who still wish to travel to Milwaukee may do so via alternate routes.",
                "Tickets can be exchanged at any ticket booth at no additional charge."],
            [
                "The purpose of this seminar is to orient businessmen and women who will be living overseas for the purpose of conducting business on behalf of their respective companies.",
                "While each country has its own particular customs, laws and culture, there are still some general rules that apply while living abroad.",
                "In the first half of the seminar, we'll be focusing on filling your taxes from abroad and how to take care of banking matters from overseas.",
                "Then we'll move on to the importance of contacting the U.S consulate or embassy and how they can assist you in an emergency.",
                "After lunch we'll come back for the second half where you'll be grouped off according to your destination country so you can talk with someone who has experienced living in that part of the world."],
            [
                "How safe is your computer from an attack?",
                "Are you running your programs on a healthy and virus-free machine?",
                "Do you worry about your identity being stolen?",
                "The only way to guarantee your computer has the complete security it needs is by ordering Samson Antivirus for your PC.",
                "This software is guaranteed to protect you from viruses and attacks by hackers.",
                "It was rated the number one antivirus software by three computer and technology magazines.",
                "With Samson, you can use the Internet and not have to worry about your credit card information ending up in the wrong hands.",
                "Our software will tell you which websites can be trusted and which ones are out to take advantage of you.",
                "Call us at 1-800-555-3330, or visit our homepage at www.samson.com for more information.",
                "We will protect you - guaranteed."],
            [
                "Ladies and Gentlemen, this is your captain speaking.",
                "As you can see from outside the window, we are in the vicinity of Newark Liberty Airport.",
                "Unfortunately, the runway is backed up and we've been directed by the tower to circle for about 10 more minutes.",
                "Once we land, passengers connecting to other flights need to take the tram located inside the terminal.",
                "Those of you for whom Newark is your final destination, please proceed to the baggage claim area.",
                "I'll be making another announcement before we begin landing.",
                "Please be reminded that the 'fasten seatbelt' sign has been turned on, and we ask you to remain seated as we will be landing shortly.",
                "Thank you."]]
        topicArrayAnswer = [
            ["What happened to Mr.Skyler's computer?",
                "Harmful programs were installed.",
                "It could not be repaired.",
                "The backup data was lost."],
            ["What does the speaker advise?",
                "Not using a credit card if you cannot pay it off soon",
                "Paying the minimum each month",
                "Withdrawing cash before making a purchase"],
            ["Who is the speaker addressing?",
                "Race participants",
                "A group of journalists",
                "Event sponsors"],
            ["What will happen to World Moving?",
                "Its name will change.",
                "Its staff will be reduced.",
                "The office will be relocated."],
            ["What problem does the speaker mention?",
                "Staff forgetting their ID cards",
                "No one answering the phone",
                "People leaving work early"],
            ["Who is the speaker addresing?",
                "Small business owners",
                "Association members",
                "Furniture makers"],
            ["When will the trains start running again?",
                "At an unknown time",
                "In a few hours",
                "After the repairs are finished"],
            ["Who will help the attendees in an emergency?",
                "Diplomatic officials",
                "Law enforcement agencies",
                "The human resources department"],
            ["What does the company claim their product will do?",
                "Keep personal data safe",
                "Help organize personal finances",
                "Reduce credit card debt"],
            ["What is happening to the plane?",
                "It is waiting to land.",
                "It is being inspected.",
                "It is being repaired."]
        ]
        
        let topicTotalNumber = 10
        randomTopic1 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic2 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic3 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic4 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic5 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic6 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic7 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic8 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic9 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        randomTopic10 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        while randomTopic1 == randomTopic2 || randomTopic1 == randomTopic3 || randomTopic1 == randomTopic4 || randomTopic1 == randomTopic5 || randomTopic1 == randomTopic6 || randomTopic1 == randomTopic7 || randomTopic1 == randomTopic8 || randomTopic1 == randomTopic9 || randomTopic1 == randomTopic10 || randomTopic2 == randomTopic3 || randomTopic2 == randomTopic4 || randomTopic2 == randomTopic5 || randomTopic2 == randomTopic6 || randomTopic2 == randomTopic7 || randomTopic2 == randomTopic8 || randomTopic2 == randomTopic9 || randomTopic2 == randomTopic10 || randomTopic3 == randomTopic4 || randomTopic3 == randomTopic5 || randomTopic3 == randomTopic6 || randomTopic3 == randomTopic7 || randomTopic3 == randomTopic8 || randomTopic3 == randomTopic9 || randomTopic3 == randomTopic10 || randomTopic4 == randomTopic5 || randomTopic4 == randomTopic6 || randomTopic4 == randomTopic7 || randomTopic4 == randomTopic8 || randomTopic4 == randomTopic9 || randomTopic4 == randomTopic10 || randomTopic5 == randomTopic6 || randomTopic5 == randomTopic7 || randomTopic5 == randomTopic8 || randomTopic5 == randomTopic9 || randomTopic5 == randomTopic10 || randomTopic6 == randomTopic7 || randomTopic6 == randomTopic8 || randomTopic6 == randomTopic9 || randomTopic6 == randomTopic10 || randomTopic7 == randomTopic8 || randomTopic7 == randomTopic9 || randomTopic7 == randomTopic10 || randomTopic8 == randomTopic9 || randomTopic8 == randomTopic10 || randomTopic9 == randomTopic10 {
            randomTopic1 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic2 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic3 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic4 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic5 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic6 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic7 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic8 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic9 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
            randomTopic10 = Int(arc4random_uniform(UInt32(topicTotalNumber)))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func startTopic(){
        if firstShowCounter == 0 {
            randomAnswer = Int(arc4random_uniform(6))
            switch numberTopic {
            case 1:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic1][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 1
                }
            case 2:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic2][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic2][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic2][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic2][3]
                    correctNumber = 1
                }
            case 3:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic3][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic3][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic3][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic3][3]
                    correctNumber = 1
                }
            case 4:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic4][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic4][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic4][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic4][3]
                    correctNumber = 1
                }
            case 5:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic5][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic5][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic5][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic5][3]
                    correctNumber = 1
                }
            case 6:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic6][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic6][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic6][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic6][3]
                    correctNumber = 1
                }
            case 7:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic7][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic7][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic7][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic7][3]
                    correctNumber = 1
                }
            case 8:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic8][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic8][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic8][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic8][3]
                    correctNumber = 1
                }
            case 9:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic9][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic9][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic9][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic9][3]
                    correctNumber = 1
                }
            case 10:
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic10][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic10][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic10][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic10][3]
                    correctNumber = 1
                }
            default :
                firstGlobalEnglishLabel = topicArrayAnswer[randomTopic1][0]
                switch randomAnswer {
                case 1:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 1
                case 2:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][2]
                    correctNumber = 1
                case 3:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 2
                case 4:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][2]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][3]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][1]
                    correctNumber = 3
                case 5:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][1]
                    correctNumber = 3
                case 6:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][3]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][1]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][2]
                    correctNumber = 2
                default:
                    globalFirstLabel = topicArrayAnswer[randomTopic1][1]
                    globalSecondLabel = topicArrayAnswer[randomTopic1][2]
                    globalThirdLabel = topicArrayAnswer[randomTopic1][3]
                    correctNumber = 1
                }
            }
        }
        englishLabel.text = firstGlobalEnglishLabel
        firstBtn.setTitle("1. " + globalFirstLabel, forState: .Normal)
        secondBtn.setTitle("2. " + globalSecondLabel, forState: .Normal)
        thirdBtn.setTitle("3. " + globalThirdLabel, forState: .Normal)
        if (timer != nil) {
            timer.invalidate()
        }
        if (speedTimer != nil) {
            speedTimer.invalidate()
        }
        if (speedInternalTimer != nil) {
            speedInternalTimer.invalidate()
        }
        if (correctTopicTimer != nil) {
            correctTopicTimer.invalidate()
        }
        if (correctIntervalTimer != nil) {
            correctIntervalTimer.invalidate()
        }
        if numberTopic == 0 {
            lastChangeSpeed = speedLevel
            finishBool = false
        }
        pauseBool = false
        canCorrect = true
        resolveTopic = false
        numberTopic++
        topicCounter.text = String(numberTopic) + "/10"
        firstShowCounter++
        correctDecisionLabel.text = ""
        deciCounter = 0
        deciTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onDeciCount"), userInfo: nil, repeats: true)
        timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
    }
    
    func onDeciCount(){
        deciCounter = deciCounter + 0.1
        progressView.progress = Float(deciCounter/5)
    }
    
    func onUpdate(){
        timer.invalidate()
        deciTimer.invalidate()
        speedTimer = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("onSpeedUpdate"), userInfo: nil, repeats: true)
        
    }
    
    func onSpeedUpdate(){
        switch numberTopic {
        case 1:
            topicLabel.text = topicArray[randomTopic1][topicArrayNumber]
            globalRandomTopic = randomTopic1
        case 2:
            topicLabel.text = topicArray[randomTopic2][topicArrayNumber]
            globalRandomTopic = randomTopic2
        case 3:
            topicLabel.text = topicArray[randomTopic3][topicArrayNumber]
            globalRandomTopic = randomTopic3
        case 4:
            topicLabel.text = topicArray[randomTopic4][topicArrayNumber]
            globalRandomTopic = randomTopic4
        case 5:
            topicLabel.text = topicArray[randomTopic5][topicArrayNumber]
            globalRandomTopic = randomTopic5
        case 6:
            topicLabel.text = topicArray[randomTopic6][topicArrayNumber]
            globalRandomTopic = randomTopic6
        case 7:
            topicLabel.text = topicArray[randomTopic7][topicArrayNumber]
            globalRandomTopic = randomTopic7
        case 8:
            topicLabel.text = topicArray[randomTopic8][topicArrayNumber]
            globalRandomTopic = randomTopic8
        case 9:
            topicLabel.text = topicArray[randomTopic9][topicArrayNumber]
            globalRandomTopic = randomTopic9
        case 10:
            topicLabel.text = topicArray[randomTopic10][topicArrayNumber]
            globalRandomTopic = randomTopic10
        default:
            topicLabel.text = topicArray[randomTopic1][topicArrayNumber]
            globalRandomTopic = randomTopic1
        }
        speedTimer.invalidate()
        speedInternalTimer = NSTimer.scheduledTimerWithTimeInterval(speedLevel, target: self, selector: Selector("holdSeconds"), userInfo: nil, repeats: true)
    }
    
    func holdSeconds(){
        topicLabel.text = ""
        speedInternalTimer.invalidate()
        if topicArrayNumber < topicArray[globalRandomTopic].count-1 {
            topicArrayNumber++
            timer = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        } else {
            deciCounter = 0
            deciTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onDeciCount"), userInfo: nil, repeats: true)
            correctTopicTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("onCorrectTopic"), userInfo: nil, repeats: true)
        }
    }
    
    func onCorrectTopic() {
        correctTopicTimer.invalidate()
        if (deciTimer != nil) {
            deciTimer.invalidate()
        }
        if resolveTopic == false {
            topicArrayNumber = 0
            correctDecisionLabel.text = "×"
            canCorrect = false
            correctIntervalTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("correctDecistionHolds"), userInfo: nil, repeats: true)
        }
        firstShowCounter = 0
    }
    
    func correctDecistionHolds(){
        topicLabel.text = ""
        firstShowCounter = 0
        if numberTopic >= 10 {
            finishBool = true
            pickerView.hidden = false
            resultBtn.hidden = false
            startButton.hidden = false
            pauseLabel.hidden = true
            resultBtn.setTitle("result ☞", forState: .Normal)
            timer.invalidate()
            canPicker = true
            if (timer != nil) {
                timer.invalidate()
            }
            if (speedTimer != nil) {
                speedTimer.invalidate()
            }
            if (speedInternalTimer != nil) {
                speedInternalTimer.invalidate()
            }
            if (correctTopicTimer != nil) {
                correctTopicTimer.invalidate()
            }
            numberTopic = 0
        }
        if canCorrect == true || finishBool == false {
            startTopic()
        }
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        print("adSet")
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if canPicker == true {
            switch row {
                case 0 :
                    speedLevel = 0.1
                case 1 :
                    speedLevel = 0.5
                case 2 :
                    speedLevel = 1.0
                case 3 :
                    speedLevel = 2.0
                default:
                    speedLevel = 0.1
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let listVC:ListViewController = segue.destinationViewController as! ListViewController
            listVC.resultPoint = String(correctTotal)
            listVC.resultSpeed = lastChangeSpeed
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
