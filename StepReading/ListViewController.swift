//
//  ListViewController.swift
//  FlashEnglish
//
//  Created by SHOKI TAKEDA on 12/10/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit
import Social

class ListViewController: UIViewController, NADViewDelegate {
    private var nadView: NADView!
    var resultPoint:String?
    var resultSpeed:Double = 0
    var adTimer:NSTimer!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultSpeedLabel: UILabel!
    
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    
    @IBAction func facebook(sender: UIButton) {
        
        let ncropRect  = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let noverViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ncropRef   = CGImageCreateWithImageInRect(noverViewImg.CGImage, ncropRect)
        let ncropImage = UIImage(CGImage: ncropRef!)
        
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Facebook?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let title: String = "#FlashReading"
            controller.setInitialText(title)
            controller.addImage(ncropImage)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func twitter(sender: UIButton) {
        let ncropRect  = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        UIGraphicsBeginImageContext(self.view.bounds.size)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let noverViewImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ncropRef   = CGImageCreateWithImageInRect(noverViewImg.CGImage, ncropRect)
        let ncropImage = UIImage(CGImage: ncropRef!)
        
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Twitter?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let title: String = "#FlashReading"
            controller.setInitialText(title)
            controller.addImage(ncropImage)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter-big.jpg"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook-big.jpg"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        resultSpeedLabel.text = String(resultSpeed) + " s"
        resultLabel.text = resultPoint! + "/10"
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        print("adSet")
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}
